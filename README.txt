
DESCRIPTION
=====================
The module makes it possible for other module developers easily to use 
Yahoo! PlaceFinder geocoding API to find geo coordinates for a given address.

The module it self don't do anything out of the box, by provides a 
function to find the geo coordinates.

INSTALLATION
=====================
Install the module from the module page in the administration, and
go to the Keys API interface and configure your Yahoo Application ID.

DEVELOPERS
=====================
Call the function yahoo_geocoding_api_geocode($address) with your address
as argument. It uses the API's location argument, see 
https://developer.yahoo.com/geo/placefinder/.

The function will return an array with latitude, longitude and accuracy.

REQUIREMENTS - 6.x-2.0
=====================
- The keys module (version 6.x-2.x only), www.drupal.org/project/keys
- A Yahoo Application ID. Sign up at https://developer.apps.yahoo.com/dashboard/createKey.html.

DRUPAL 7
=====================
In development. Waiting on Drupal 7 version of the keys module.
                                                 
DRUPAL 5
=====================
No support planned.

CONTRIBUTORS
=====================
Jens Beltofte Sørensen - http://drupal.org/user/151799

SPONSORS
=====================
Propeople ApS (www.propeople.dk)
